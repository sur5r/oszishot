/*
 * This file is part of oszishot.
 *
 * oszishot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * oszishot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with oszishot.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011 Jakob Haufe <sur5r@sur5r.net>
 *
 */


#define _DEFAULT_SOURCE
#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <poll.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <string.h>
#include <sys/mman.h>


void detectresolution(char *filename, int *resolution)
{
    int fd;
    void *map;
    char *work;

    fd=open(filename, O_RDONLY);
    if(fd<0)
    {
        perror("open");
        return;
    }
    map = mmap(NULL, 64, PROT_READ, MAP_SHARED, fd, 0);
    if(map == MAP_FAILED)
    {
        perror("mmap");
        close(fd);
        return;
    }
    work=(char*)map;
    while(work < (char*)map+57)
    {
        if(work[0]==0x1b && work[1]=='*' && work[2]=='t' && ( work[5] == 'R' || work[6]=='R' || work[7]=='R'))
        {
            *resolution=atoi(&work[3]);
            printf("Detected resolution: %d\n", *resolution);
            break;
        }
        work++;
    }
    munmap(map, 64);
    close(fd);
}

int main(int argc, char **argv)
{
    int retval, serfd;
    int filefd;
    time_t t;
    struct tm *tm;
    char filename[20]; // 20111023 _ 234213 . pcl \0
    char command[256];
    void *buf;
    struct termios termios;
    struct pollfd pfd;

    char *port;
    int baud=19200, res=100;
    speed_t speed;

    if(argc < 2)
    {
        fprintf(stderr,
                "Usage: %s port [[baudrate] resolution]\n"
                "Baudrate: 110, 300, 600, 1200, 2400, 4800, 9600, 19200 (default).\n"
                "Resolution: used as fallback if autodetect fails, defaults to 100\n"
                ,argv[0]
                );
        exit(EXIT_FAILURE);
    }

    port = strdupa(argv[1]);
    if(argc > 2)
    {
        baud = atoi(argv[2]);
        if(argc > 3)
        {
            res = atoi(argv[3]);
        }
    }

    printf("Capturing screenshots on %s with %d bps and converting using"
           " resolution %d\n", port, baud, res);

    buf = malloc(1024);
    if(!buf)
    {
        perror("malloc");
        exit(EXIT_FAILURE);
    }

    serfd=open(port, O_RDONLY); 
    if(serfd<0)
    {
        perror("open");
        exit(EXIT_FAILURE);
    }

    pfd.fd=serfd;
    pfd.events=POLLIN | POLLERR | POLLHUP;


    tcgetattr(serfd, &termios);

    cfmakeraw(&termios);

    switch(baud)
    {
        case 110:
            speed=B110;
            break; 

        case 300:
            speed=B300;
            break;

        case 600:
            speed=B600;
            break;

        case 1200:
            speed=B1200;
            break;

        case 2400:
            speed=B2400;
            break;

        case 4800:
            speed=B4800;
            break;

        case 9600:
            speed=B9600;
            break;

        case 19200:
            speed=B19200;
            break;

        default:
            fprintf(stderr,"Invalid baud rate %d\n", baud);
            exit(EXIT_FAILURE);
    }
    cfsetospeed(&termios, speed);
    cfsetispeed(&termios, speed);

    tcsetattr(serfd, TCSANOW, &termios);

    while(1)
    {
        retval = poll(&pfd,1, -1);

        if(retval != 1)
        {
            perror("poll");
            exit(EXIT_FAILURE);
        }
        else
        {
            if(pfd.revents & POLLERR)
            {
                perror("poll");
                exit(EXIT_FAILURE);
            }

            t = time(NULL);
            tm = localtime(&t);
            if(tm == NULL)
            {
                perror("localtime");
                exit(EXIT_FAILURE);
            }

            strftime(filename, 20, "%Y%m%d_%H%M%S.pcl", tm);
            filefd=creat(filename, S_IRUSR | S_IWUSR);
            if(filefd<0)
            {
                perror("creat");
                exit(EXIT_FAILURE);
            }
            printf("Writing screenshot to %s...\n", filename);

            int totalbytes=0;

            while(poll(&pfd,1,1000) > 0)
            {
                if(pfd.revents & POLLERR)
                {
                    perror("poll");
                    exit(EXIT_FAILURE);
                }
                int nbytes;
                nbytes = read(serfd, buf, 1024);
                totalbytes+=nbytes;
                write(filefd, buf, nbytes);
            }
            printf(" %d bytes written\n", totalbytes);

            close(filefd);
            detectresolution(filename, &res);

            snprintf(command, 256, "pcl6 -dNOPAUSE -r%d -sDEVICE=pngmono -sOutputFile=%s.%%d.png %s", res, filename, filename);
            printf("Running: %s\n",command);

            system(command);
        }
    }

}
